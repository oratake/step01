// common.js
/*
Standards Compliant Rollover Script
Author : Daniel Nolan
http://www.bleedingego.co.uk/webdev.php
*/
var preLoadImg = new Object();
function initRollOvers(){
	$("img.img_on").each(function(){
		var imgSrc = this.src;
		var sep = imgSrc.lastIndexOf('.');
		var onSrc = imgSrc.substr(0, sep) + '_on' + imgSrc.substr(sep, 4);
		preLoadImg[imgSrc] = new Image();
		preLoadImg[imgSrc].src = onSrc;
		$(this).hover(
			function() { this.src = onSrc; },
			function() { this.src = imgSrc; }
		);
	});
}
// rolloverの実行
$(function(){
	initRollOvers();
});

// スムーススクロール
$(function(){
	$('a[href^=#]:not(.no_scroll)').click(function(){
		var headerHight = 0; // 固定ヘッダの高さ
		var speed = 500; // スクロールの速さ
		var href= $(this).attr("href");
		var target = $(href == "#" || href == "" ? 'html' : href);
		var position = target.offset().top-headerHight;
		$("html, body").animate({scrollTop:position}, speed, "swing");
		return false;
	});
});

// ユーザーエージェントチェック
var agent = navigator.userAgent;

var spFlag = false;
if(agent.indexOf("iPhone") > 0){
	// iphone処理
	spFlag = true;
}else if(agent.indexOf("Android") > 0){
	// android処理
	spFlag = true;
}


// 電話リンクの削除
$(function(){
	if(!spFlag){
		$("a[href^='tel:']").each(function(){
		
		var replaceText = 'javascript:void(0);';
			$(this).attr("href", replaceText);
			$(this).css({"cursor": "default"});
		});
	}
});
//============================================================
//	サイト毎に共通動作を記述する場合はここより下で行うこと
//============================================================
